// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAMEPROJECT_API ASnakeGameProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
